// alert function
function sweetalert_reload(response){
	if (response.action === 'success') {
		swal({
			title: 'Success !',
			html: response.message,
			type: 'success',
			showCancelButton: false,
			cancelButtonClass: 'btn-default btn-md waves-effect',
			confirmButtonClass: 'btn-success btn-md waves-effect waves-light',
			confirmButtonText: 'OK'
		},
		function() {
			location.reload(true);
		});
	}
	else if (response.action === 'error') {
		swal({
			title: 'Error !',
			html: response.message,
			type: 'error',
			showCancelButton: false,
			cancelButtonClass: 'btn-default btn-md waves-effect',
			confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
			confirmButtonText: 'OK'
		},
		function() {
			location.reload(true);
		});
	}
	else if (response.action === 'warning') {
		swal({
			title: 'Warning !',
			html: response.message,
			type: 'warning',
			showCancelButton: false,
			cancelButtonClass: 'btn-default btn-md waves-effect',
			confirmButtonClass: 'btn-warning btn-md waves-effect waves-light',
			confirmButtonText: 'OK'
		},
		function() {
			location.reload(true);
		});
	}
} // end alert function
