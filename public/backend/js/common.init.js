(function( $ ) {
    'use strict';
    $(function() {
        // add _apply attribute
        $('#apply').on('click', function() {
            $('#form').append('<input name="_apply" type="hidden" value="true">');
        });

        // check/uncheck all
        $('#datatable-tabletools').on('click', '#check-all', function() {
            if(this.checked) {
                $('.checkbox').each(function() {
                    this.checked = true;
                });
            }
            else {
                $('.checkbox').each(function() {
                    this.checked = false;
                });
            }
        }); // end check/uncheck all

		// prosessing icon
		$('.load').on('click', function() {
			var $this = $(this);
			$this.button('loading');
			$this.tooltip('destroy')

			setTimeout(function() {
			   $this.button('reset');
			}, 8000);
		});

        // activate state / deactivate state
		$('#datatable-tabletools').on('click', '.stateBtn', function() {
  			var state_url = $(this).data('state');
  			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: state_url,
				type: 'GET',
				dataType: 'json',
	            success: function(result) {
	                sweetalert_reload(result);
	            }
	        }) // ajax
  			return false;
		}); // end activate state / end deactivate state

        // restore state
		$('#datatable-tabletools').on('click', '.restoreBtn', function() {
  			var restore_url = $(this).data('restore');
  			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: restore_url,
				type: 'GET',
				dataType: 'json',
                success: function(result) {
					sweetalert_reload(result);
                }
            }) // ajax
      		return false;
		}); // end restore state

        // destroy state
		$('#datatable-tabletools').on('click', '.destroyBtn', function() {
  			var destroy_url = $(this).data('destroy');
  			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: destroy_url,
				type: 'GET',
				dataType: 'json',
	            success: function(result) {
					sweetalert_reload(result);
            	}
	        }) // ajax
  			return false;
		}); // end destroy state

        // core menu
        $('#core-menu').ready(function() {
            // get path from data-path
            var path = $('#core-menu').data('path');

            // activate state
            $('#activate').on('click', function() {

                // get checked & unchecked
                var checked_id = $('input:checkbox:checked').map(function() {
                    if($.isNumeric(this.value)) {
                        return this.value;
                    }
                }).get().join();

                // check empty data
                if ( checked_id ) {
                    swal({
                        title: 'Are you sure ?',
                        html: 'Do you want to <span class="text-success">activate</span> record id <b>#'+checked_id+'</b> ?',
                        type: 'warning',
                        showCancelButton: true,
                        cancelButtonClass: 'btn-default btn-md waves-effect',
                        confirmButtonClass: 'btn-warning btn-md waves-effect waves-light',
                        confirmButtonText: 'Yes, activate it!',
                        closeOnConfirm: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                url: base_url+'/'+path+'/state/'+checked_id+'/1',
                                type: 'GET',
                                dataType: 'json',
                                success: function(result) {
									sweetalert_reload(result);
                                }
                            }) // ajax
                            return false;
                        }
                    });
                }
                else {
                    swal({
                        title: 'Error !',
                        html: 'Please select one of those item to <span class="text-success">activate</span>.',
                        type: 'error',
                        showCancelButton: false,
                        cancelButtonClass: 'btn-default btn-md waves-effect',
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: 'OK',
                    });
                }
            }); // end activate state

            // deactivate state
            $('#deactivate').on('click', function() {

                // get checked & unchecked
                var checked_id = $('input:checkbox:checked').map(function() {
                    if($.isNumeric(this.value)) {
                        return this.value;
                    }
                }).get().join();

                // check empty data
                if ( checked_id ) {
                    swal({
                        title: 'Are you sure ?',
                        html: 'Do you want to <span class="text-brown">deactivate</span> record id <b>#'+checked_id+'</b> ?',
                        type: 'warning',
                        showCancelButton: true,
                        cancelButtonClass: 'btn-default btn-md waves-effect',
                        confirmButtonClass: 'btn-warning btn-md waves-effect waves-light',
                        confirmButtonText: 'Yes, deactivate it!',
                        closeOnConfirm: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                url: base_url+'/'+path+'/state/'+checked_id+'/0',
                                type: 'GET',
                                dataType: 'json',
                                success: function(result) {
									sweetalert_reload(result);
                                }
                            }) // ajax
                            return false;
                        }
                    });
                }
                else {
                    swal({
                        title: 'Error !',
                        html: 'Please select one of those item to <span class="text-brown">deactivate</span>.',
                        type: 'error',
                        showCancelButton: false,
                        cancelButtonClass: 'btn-default btn-md waves-effect',
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: 'OK',
                    });
                }
            }); // end deactivate state

            // remove data to trash
            $('#remove').on('click', function() {

                // get checked & unchecked
                var checked_id = $('input:checkbox:checked').map(function() {
                    if($.isNumeric(this.value)) {
                        return this.value;
                    }
                }).get().join();

                // check empty data
                if ( checked_id ) {
                    swal({
                        title: 'Are you sure ?',
                        html: 'Do you want to <span class="text-danger">remove</span> record id <b>#'+checked_id+'</b> ?',
                        type: 'warning',
                        showCancelButton: true,
                        cancelButtonClass: 'btn-default btn-md waves-effect',
                        confirmButtonClass: 'btn-warning btn-md waves-effect waves-light',
                        confirmButtonText: 'Yes, remove it!',
                        closeOnConfirm: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                url: base_url+'/'+path+'/remove/'+checked_id,
                                type: 'GET',
                                dataType: 'json',
                                success: function(result) {
									sweetalert_reload(result);
                                }
                            }) // ajax
                            return false;
                        }
                    });
                }
                else {
                    swal({
                        title: 'Error !',
                        html: 'Please select one of those item to <span class="text-danger">remove</span>.',
                        type: 'error',
                        showCancelButton: false,
                        cancelButtonClass: 'btn-default btn-md waves-effect',
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: 'OK',
                    });
                }
            }); // end data to trash

            // restore data from trash
            $('#restore').on('click', function() {

                // get checked & unchecked
                var checked_id = $('input:checkbox:checked').map(function() {
                    if($.isNumeric(this.value)) {
                        return this.value;
                    }
                }).get().join();

                // check empty data
                if ( checked_id ) {
                    swal({
                        title: 'Are you sure ?',
                        html: 'Do you want to <span class="text-primary">restore</span> record id <b>#'+checked_id+'</b> ?',
                        type: 'warning',
                        showCancelButton: true,
                        cancelButtonClass: 'btn-default btn-md waves-effect',
                        confirmButtonClass: 'btn-warning btn-md waves-effect waves-light',
                        confirmButtonText: 'Yes, restore it!',
                        closeOnConfirm: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                url: base_url+'/'+path+'/restore/'+checked_id,
                                type: 'GET',
                                dataType: 'json',
                                success: function(result) {
									sweetalert_reload(result);
                                }
                            }) // ajax
                            return false;
                        }
                    });
                }
                else {
                    swal({
                        title: 'Error !',
                        html: 'Please select one of those item to <span class="text-primary">restore</span>.',
                        type: 'error',
                        showCancelButton: false,
                        cancelButtonClass: 'btn-default btn-md waves-effect',
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: 'OK',
                    });
                }
            }); // end restore data from trash

            // destroy data from trash
            $('#destroy').on('click', function() {

                // get checked & unchecked
                var checked_id = $('input:checkbox:checked').map(function() {
                    if($.isNumeric(this.value)) {
                        return this.value;
                    }
                }).get().join();

                // check empty data
                if ( checked_id ) {
                    swal({
                        title: 'Are you sure ?',
                        html: 'Do you want to <span class="text-danger">destroy</span> record id <b>#'+checked_id+'</b> ?',
                        type: 'warning',
                        showCancelButton: true,
                        cancelButtonClass: 'btn-default btn-md waves-effect',
                        confirmButtonClass: 'btn-warning btn-md waves-effect waves-light',
                        confirmButtonText: 'Yes, destroy it!',
                        closeOnConfirm: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                url: base_url+'/'+path+'/destroy/'+checked_id,
                                type: 'GET',
                                dataType: 'json',
                                success: function(result) {
									sweetalert_reload(result);
                                }
                            }) // ajax
                            return false;
                        }
                    });
                }
                else {
                    swal({
                        title: 'Error !',
                        html: 'Please select one of those item to <span class="text-danger">destroy</span>.',
                        type: 'error',
                        showCancelButton: false,
                        cancelButtonClass: 'btn-default btn-md waves-effect',
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: 'OK',
                    });
                }
            }); // end destroy data from trash

        }); // end index button
        
	}); // $(function()
}).apply( this, [ jQuery ]);
