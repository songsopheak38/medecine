(function( $ ) {
	'use strict';
	$(function() {
		Dropzone.autoDiscover = false;
		$("div#featured-dropzone").dropzone({
			url: $("div#featured-dropzone").data('upload'),
			maxFiles: 1,
			maxFilesize: 2, // MB
			thumbnailWidth: 300,
			acceptedFiles: 'image/*',
			dictDefaultMessage: '<h2>Drop image here</h2><h3>to upload</h3><h4>(or Click)</h4>',
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			init: function() {
				this.on('success', function(file, response) {
					var obj_image = $('#preview').find('img');
					var url = obj_image.data('url');
					obj_image.attr('src', url+'/'+response.temppath+'/'+response.filename);
					$('#form').append('<input type="hidden" name="image" value="'+response.filename+'">'+
														'<input type="hidden" name="temppath" value="'+response.temppath+'">');
					this.removeAllFiles();
					$('#upload').hide();
					$('#preview').fadeIn();
					sweetalert(response);
				});
				this.on("error", function(file, response) {
					this.removeAllFiles();
					$('#upload').fadeIn();
					$('#preview').hide();
					sweetalert(response);
				});
			} // end init
		}); //end #featured-dropzone
		$("div#featured-dropzone1").dropzone({
			url: $("div#featured-dropzone1").data('upload'),
			maxFiles: 1,
			maxFilesize: 2, // MB
			thumbnailWidth: 300,
			acceptedFiles: 'image/*',
			dictDefaultMessage: '<h2>Drop image here</h2><h3>to upload</h3><h4>(or Click)</h4>',
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			init: function() {
				this.on('success', function(file, response) {
					var obj_image = $('#previews').find('img');
					var url = obj_image.data('url');
					obj_image.attr('src', url+'/'+response.temppath+'/'+response.filename);
					$('#form').append('<input type="hidden" name="image_company" value="'+response.filename+'">'+
														'<input type="hidden" name="temppath" value="'+response.temppath+'">');
					this.removeAllFiles();
					$('#uploads').hide();
					$('#previews').fadeIn();
					sweetalert(response);
				});
				this.on("error", function(file, response) {
					this.removeAllFiles();
					$('#uploads').fadeIn();
					$('#previews').hide();
					sweetalert(response);
				});
			} // end init
		}); //end #featured-dropzone

		$('#featured-remove').on('click', function(){
			$.ajax({
				type: 'POST',
				url: $('#featured-remove').data('remove'),
				data: {
					file_image:	$('#form').find('input[name="image"]').val(),
					file_path:	$('#form').find('input[name="temppath"]').val()
				},
				dataType: 'html',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				success: function(response){
					var obj = JSON.parse(response);
					sweetalert(obj);
					if (obj.action === 'success') {
						$('#upload').fadeIn();
						$('#preview').hide();
						$('#form').find('input[name="image"]').remove();
						$('#form').find('input[name="temppath"]').remove();
					}
				}
			});
		}); // end #featured-remove
		$('#featured-remove1').on('click', function(){
			$.ajax({
				type: 'POST',
				url: $('#featured-remove1').data('remove'),
				data: {
					file_image:	$('#form').find('input[name="image_company"]').val(),
					file_path:	$('#form').find('input[name="temppath"]').val()
				},
				dataType: 'html',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				success: function(response){
					var obj = JSON.parse(response);
					sweetalert(obj);
					if (obj.action === 'success') {
						$('#uploads').fadeIn();
						$('#previews').hide();
						$('#form').find('input[name="image_company"]').remove();
						$('#form').find('input[name="temppath"]').remove();
					}
				}
			});
		}); // end #featured-remove




	//
	//
	// 	$("div#photos-dropzone").dropzone({
	// 		url: $("div#photos-dropzone").data('upload'),
	// 		maxFilesize: 2, // MB
	// 		thumbnailWidth: 215,
	// 		acceptedFiles: 'image/*',
	// 		dictDefaultMessage: '<h2>Drop images here</h2><h3>to upload</h3><h4>(or Click)</h4>',
	// 		headers: {
	// 			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	// 		},
	// 		addRemoveLinks: true,
	// 		init: function() {
	// 			this.on('success', function(file, response) {
	//
	// 				var str = file.name.replace('.', '-');
	// 				var idimage = str.replace(' ', '-');
	//
	// 				$('#form').append('<div class="'+idimage+'"><input type="hidden" name="photos[]" value="'+response.filename+'">'+
	// 						'<input type="hidden" name="temppath" value="'+response.temppath+'"></div>');
	//
	// 			});
	// 			this.on("removedfile", function(file) {
	// 				var str = file.name.replace('.', '-');
	// 				var idimage = str.replace(' ', '-');
	// 				$('.'+idimage).remove();
	// 			});
	// 		}
	// 	});
	//
	// 	$('.photo-remove').on('click', function(e) {
	// 		e.preventDefault();
	// 		NProgress.configure({ easing: 'ease', speed: 500, trickleRate: 0.02, trickleSpeed: 500 });
	// 		NProgress.set(0.4);
	//
	// 		var $this = $( this );
	//
	// 		swal({
	// 			title: 'Are you sure?',
	// 			html: 'Do you want to <b class="text-danger">remove</b> photo?',
	// 			type: 'warning',
	// 			showCancelButton: true,
	// 			confirmButtonColor: '#3085d6',
	// 			cancelButtonColor: '#d33',
	// 			confirmButtonText: 'Yes, remove it!',
	// 			closeOnConfirm: true
	// 		},
	// 		function(isConfirm) {
	// 			if (isConfirm) {
	// 				NProgress.set(0.6);
	// 				$this.parent().remove();
	// 				$.ajax({
	// 					headers: {
	// 						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	// 					},
	// 					url: $this.data('remove'),
	// 					type: 'POST',
	// 					dataType: 'json',
	// 					data: {
	// 						file_image:	$this.next().val(),
	// 						file_path:	$this.next().next().val()
	// 					},
	//
	// 					success: function(result) {
	// 						if (result.action === 'success') {
	// 							swal({
	// 								title: 'Success!',
	// 								html: result.message,
	// 								type: 'success',
	// 							},
	// 							function() {
	// 								NProgress.done();
	// 							});
	// 						} else if (result.action === 'error') {
	// 							swal({
	// 								title: 'Error!',
	// 								html: result.message,
	// 								type: 'error',
	// 							},
	// 							function() {
	// 								NProgress.done();
	// 							});
	// 						} else if (result.action === 'warning') {
	// 							swal({
	// 								title: 'Warning!',
	// 								html: result.message,
	// 								type: 'warning',
	// 							},
	// 							function() {
	// 								NProgress.done();
	// 							});
	// 						}
	// 					},
	//
	// 					/*
	// 					error: function (xhr, ajaxOptions, thrownError) {
	// 						location.reload(true);
	// 					}
	// 					*/
	// 				})
	// 				return false;
	// 			}
	// 			else {
	// 				NProgress.done(true);
	// 			}
	// 		});
  // });

		// alert function
		function sweetalert(response){
			if (response.action === 'success') {
				swal({
					title: 'Success !',
					html: response.message,
					type: 'success',
					showCancelButton: false,
			        cancelButtonClass: 'btn-default btn-md waves-effect',
			        confirmButtonClass: 'btn-success btn-md waves-effect waves-light',
			        confirmButtonText: 'OK'
				});
			}
			else if (response.action === 'error') {
				swal({
					title: 'Error !',
					html: response.message,
					type: 'error',
					showCancelButton: false,
			        cancelButtonClass: 'btn-default btn-md waves-effect',
			        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
			        confirmButtonText: 'OK'
				});
			}
			else if (response.action === 'warning') {
				swal({
					title: 'Warning !',
					html: response.message,
					type: 'warning',
					showCancelButton: false,
			        cancelButtonClass: 'btn-default btn-md waves-effect',
			        confirmButtonClass: 'btn-warning btn-md waves-effect waves-light',
			        confirmButtonText: 'OK'
				});
			}
		} // end alert function
	}); // function
}).apply( this, [ jQuery ]);
