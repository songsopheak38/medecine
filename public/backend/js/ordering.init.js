// Ordering Function

// Initialise default ordering
function drag_drop_ordering(url, dbtbl){
	$("#datatable-tabletools").tableDnD({
	    onDrop: function(table, row) {
			var rows = table.tBodies[0].rows;
			// loop to get (ordering and id) number
			var order_number = []; var id_number = [];
			for (var i=0; i<rows.length; i++) {
				if ($(rows[i]).hasClass('orderSelector')) {
					order_number.push($(rows[i]).attr('data-order'));
					id_number.push($(rows[i]).attr('data-id'));
				}
			}
			// short order number acs
			order_number.sort(function(a, b){return b-a})
			// join (ordering andid) number
			var cond = ''; var keys = '';
			for (var i = 0; i < id_number.length; i++) {
				cond += 'WHEN '+id_number[i]+' THEN '+order_number[i]+' ';
				keys += id_number[i]+',';
			}
			// trim, replace space and comma
			var cond = cond.trim();
			var keys = keys.replace(/(^,)|(,$)/g, "");
			// do save ordering
			save_ordering(url, dbtbl, keys, cond);
	    },
        dragHandle: ".dragHandle"
	});

	// add show class by class dragHandle
	$("#datatable-tabletools tr").hover(function() {
		$(this).find('.dragHandle').addClass('showDragHandle');
	}, function() {
		$(this).find('.dragHandle').removeClass('showDragHandle');
	});
}

// save to database
function save_ordering(url, dbtbl, keys, cond){
	$(function() {
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			type: 'POST',
			dataType: 'json',
			url: url,
			data: {
				'table': dbtbl,
				'data': {'ids': keys, 'ordering': cond}
			},
			success: function(result) {
				sweetalert_reload(result);
			}
		});
	});
}
