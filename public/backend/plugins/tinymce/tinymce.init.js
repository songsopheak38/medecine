if($("#small-tinymce-km").length > 0){
    tinymce.init({
    	selector: "textarea#small-tinymce-km",
    	theme: "modern",
    	height: "150",
    	menubar: "false",
        plugins: [
             "link, anchor, preview, code, textcolor, fullscreen"
       ],
       toolbar: "bold italic underline strikethrough | forecolor backcolor | alignleft aligncenter alignright alignjustify | link unlink | preview code fullscreen",
       style_formats: [
            {title: 'Bold text', inline: 'b'},
            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
            {title: 'Example 1', inline: 'span', classes: 'example1'},
            {title: 'Example 2', inline: 'span', classes: 'example2'},
            {title: 'Table styles'},
            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
        ]
    });
}
if($("#small-tinymce-en").length > 0){
    tinymce.init({
    	selector: "textarea#small-tinymce-en",
    	theme: "modern",
    	height: "150",
    	menubar: "false",
        plugins: [
             "link, anchor, preview, code, textcolor, fullscreen"
       ],
       toolbar: "bold italic underline strikethrough | forecolor backcolor | alignleft aligncenter alignright alignjustify | link unlink | preview code fullscreen",
       style_formats: [
            {title: 'Bold text', inline: 'b'},
            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
            {title: 'Example 1', inline: 'span', classes: 'example1'},
            {title: 'Example 2', inline: 'span', classes: 'example2'},
            {title: 'Table styles'},
            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
        ]
    });
}
if($("#small-tinymce-ja").length > 0){
    tinymce.init({
    	selector: "textarea#small-tinymce-ja",
    	theme: "modern",
    	height: "150",
    	menubar: "false",
        plugins: [
             "link, anchor, preview, code, textcolor, fullscreen"
       ],
       toolbar: "bold italic underline strikethrough | forecolor backcolor | alignleft aligncenter alignright alignjustify | link unlink | preview code fullscreen",
       style_formats: [
            {title: 'Bold text', inline: 'b'},
            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
            {title: 'Example 1', inline: 'span', classes: 'example1'},
            {title: 'Example 2', inline: 'span', classes: 'example2'},
            {title: 'Table styles'},
            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
        ]
    });
}
if($("#big-tinymce-km").length > 0){
    var big_editor_config_km = {
        path_absolute : "/admin",
        selector: "textarea#big-tinymce-km",
        theme: "modern",
        height: "300",
    	menubar: "false",
        plugins: [
            "image, media, link, anchor, preview, code, table, textcolor, fullscreen, searchreplace"
        ],
        toolbar: "insertfile undo redo | styleselect | forecolor backcolor | bullist numlist | outdent indent | table searchreplace | image media | link unlink anchor | preview code fullscreen",
        style_formats: [
            {title: 'Bold text', inline: 'b'},
            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
            {title: 'Example 1', inline: 'span', classes: 'example1'},
            {title: 'Example 2', inline: 'span', classes: 'example2'},
            {title: 'Table styles'},
            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
        ],
        relative_urls: false,
        file_browser_callback : function(field_name, url, type, win) {
          var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
          var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
    
          var cmsURL = big_editor_config_km.path_absolute + '/laravel-filemanager?field_name=' + field_name;
          if (type == 'image') {
            cmsURL = cmsURL + "&type=Images";
          } else {
            cmsURL = cmsURL + "&type=Files";
          }    
          tinyMCE.activeEditor.windowManager.open({
            file : cmsURL,
            title : 'Filemanager',
            width : x * 0.8,
            height : y * 0.8,
            resizable : "yes",
            close_previous : "no"
          });
        }
      };    
      tinymce.init(big_editor_config_km);
}
if($("#big-tinymce-en").length > 0){
    var big_editor_config_en = {
        path_absolute : "/admin",
        selector: "textarea#big-tinymce-en",
        theme: "modern",
        height: "300",
    	menubar: "false",
        plugins: [
            "image, media, link, anchor, preview, code, table, textcolor, fullscreen, searchreplace"
        ],
        toolbar: "insertfile undo redo | styleselect | forecolor backcolor | bullist numlist | outdent indent | table searchreplace | image media | link unlink anchor | preview code fullscreen",
        style_formats: [
            {title: 'Bold text', inline: 'b'},
            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
            {title: 'Example 1', inline: 'span', classes: 'example1'},
            {title: 'Example 2', inline: 'span', classes: 'example2'},
            {title: 'Table styles'},
            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
        ],
        relative_urls: false,
        file_browser_callback : function(field_name, url, type, win) {
          var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
          var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
    
          var cmsURL = big_editor_config_en.path_absolute + '/laravel-filemanager?field_name=' + field_name;
          if (type == 'image') {
            cmsURL = cmsURL + "&type=Images";
          } else {
            cmsURL = cmsURL + "&type=Files";
          }    
          tinyMCE.activeEditor.windowManager.open({
            file : cmsURL,
            title : 'Filemanager',
            width : x * 0.8,
            height : y * 0.8,
            resizable : "yes",
            close_previous : "no"
          });
        }
      };    
      tinymce.init(big_editor_config_en);
}
if($("#big-tinymce-ja").length > 0){
    var big_editor_config_ja = {
        path_absolute : "/admin",
        selector: "textarea#big-tinymce-ja",
        theme: "modern",
        height: "300",
    	menubar: "false",
        plugins: [
            "image, media, link, anchor, preview, code, table, textcolor, fullscreen, searchreplace"
        ],
        toolbar: "insertfile undo redo | styleselect | forecolor backcolor | bullist numlist | outdent indent | table searchreplace | image media | link unlink anchor | preview code fullscreen",
        style_formats: [
            {title: 'Bold text', inline: 'b'},
            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
            {title: 'Example 1', inline: 'span', classes: 'example1'},
            {title: 'Example 2', inline: 'span', classes: 'example2'},
            {title: 'Table styles'},
            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
        ],
        relative_urls: false,
        file_browser_callback : function(field_name, url, type, win) {
          var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
          var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
    
          var cmsURL = big_editor_config_ja.path_absolute + '/laravel-filemanager?field_name=' + field_name;
          if (type == 'image') {
            cmsURL = cmsURL + "&type=Images";
          } else {
            cmsURL = cmsURL + "&type=Files";
          }    
          tinyMCE.activeEditor.windowManager.open({
            file : cmsURL,
            title : 'Filemanager',
            width : x * 0.8,
            height : y * 0.8,
            resizable : "yes",
            close_previous : "no"
          });
        }
      };    
      tinymce.init(big_editor_config_ja);
}