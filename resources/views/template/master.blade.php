<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
{{--        <title>{{ $title ?? '' }} {{ trans('translate.tagline') }}</title>--}}
{{--        <meta name="description" content="{{ $description ?? '' }}">--}}
{{--        <meta name="keywords" content="{{ $keyword ?? '' }}">--}}
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="{{ asset('img/favicons/favicon.ico') }}" type="image/x-icon">
        <link rel="apple-touch-icon-precomposed" href="{{ asset('img/favicons/apple-touch-icon.png') }}">

        <!-- Twitter Card data -->
        <meta name="twitter:card" value="summary">

        <!-- Open Graph data -->
{{--        <meta property="og:title" content="{{ $title ?? '' }}" />--}}
        <meta property="og:type" content="article" />
        <meta property="og:url" content="{{ URL::current() }}" />
{{--        <meta property="og:image" content="{{ $image ?? '' }}" />--}}
{{--        <meta property="og:description" content="{{ $description ?? '' }}" />--}}

        {{-- <link rel="stylesheet" href="css/slick/slick.css">
        <link rel="stylesheet" href="css/slick/slick-theme.css"> --}}

        <link rel="stylesheet" href="{{ asset('css/fonticons.css') }}">
        <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
        <link rel="stylesheet" href="{{ asset('css/bootsnav.css') }}">

{{--        @if ($css)--}}
{{--            @for ($i = 0; $i < count($css); $i++)--}}
{{--                <link rel="stylesheet" type="text/css" href="{{ asset($css[$i]) }}">--}}
{{--            @endfor--}}
{{--        @endif--}}

        <!--For Plugins external css-->
        <link rel="stylesheet" href="{{ asset('css/plugins.css') }}" />

        <!--Theme custom css -->
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

        <!-- Colors css -->
        <link rel="stylesheet" href="{{ asset('css/colors/blue.css') }}">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="{{ asset('css/responsive.css') }}" />
        <script src="{{ asset('js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>

        <!-- Load Facebook SDK for JavaScript -->
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

    </head>
    <body data-spy="scroll" data-target=".navbar-collapse">

        <div class='preloader'>
            <div class='loaded atebits-loader'>&nbsp;</div>
        </div>

        <div class="culmn home1">
            <!-- start nav -->
            <nav class="navbar home1_menu navbar-default navbar-fixed navbar-mobile bootsnav">
                <!-- trade summary -->
                <div class="container">
                    <div class="row">
                        <div class="head_top_social_area">
                            <div class="head_top_social">
                                <div class="col-sm-3 pl-0">
                                    <div class="ti-title">
                                        Trade Summary : {{ Session::get('trade_date') }}
                                    </div>
                                </div>
                                <div class="col-sm-7 pl-0">
                                    <marquee class="top-marquee" height="35" scrollamount="4" behavior="alternate" onmouseover="this.stop();" onmouseout="this.start();">
                                        <span style="color:#259CC2">Last Price&nbsp;:&nbsp;KHR&nbsp;{{ Session::get('last_price') }},&nbsp;&nbsp;</span>
                                        <span style="color:#CD5360">Change&nbsp;:&nbsp;KHR&nbsp;{{ Session::get('change_price') }},&nbsp;&nbsp;</span>
                                        <span style="color:#FAB704">Change&nbsp;:&nbsp;%&nbsp;{{ Session::get('change_percentage') }},&nbsp;&nbsp;</span>
                                        <span style="color:#F9FCFE">P/E Ratio&nbsp;:&nbsp;{{ Session::get('pe_ratio') }},&nbsp;&nbsp;</span>
                                        <span style="color:#259CC2">Base Price&nbsp;:&nbsp;KHR&nbsp;{{ Session::get('base_price') }},&nbsp;&nbsp;</span>
                                        <span style="color:#CD5360">Open Price&nbsp;:&nbsp;KHR&nbsp;{{ Session::get('open_price') }},&nbsp;&nbsp;</span>
                                        <span style="color:#FAB704">Close Price&nbsp;:&nbsp;KHR&nbsp;{{ Session::get('close_price') }},&nbsp;&nbsp;</span>
                                        <span style="color:#F9FCFE">High Price&nbsp;:&nbsp;KHR&nbsp;{{ Session::get('high_price') }},&nbsp;&nbsp;</span>
                                        <span style="color:#259CC2">Low Price&nbsp;:&nbsp;KHR&nbsp;{{ Session::get('low_price') }},&nbsp; </span>
                                        <span style="color:#CD5360">Accumulating Volume&nbsp;:&nbsp;{{ Session::get('accumulating_volume') }},&nbsp;&nbsp;</span>
                                        <span style="color:#FAB704">Accumulating Value&nbsp;:&nbsp;KHR&nbsp;{{ Session::get('accumulating_value') }}</span>
                                    </marquee>
                                </div>
{{--                                <div class="col-sm-2">--}}
{{--                                    @include('layout.language')--}}
{{--                                </div>--}}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end of trade summary -->

                <!-- contact info -->
                <div class="container">
                    <div class="row">
                        <div class="nav-top clearfix">
                            <div class="logo">
                            </div>
                            <div class="call_us_area">
                                <ul class="list-inline pull-right text-left">
                                    <li>
                                        <span class="icon icon-Phone2"></span>
                                        <span>{{ trans('translate.txt_telephone') }}<br /> {{ trans('translate.office') }} / {{ trans('translate.telephone') }}</span>
                                    </li>
                                    <li>
                                        <span class="icon icon-FileBox"></span>
                                        <span>{{ trans('translate.txt_fax') }}<br /> {{ trans('translate.fax') }}</span>
                                    </li>
                                    <li>
                                        <span class="icon icon-Mail"></span>
                                        <span>{{ trans('translate.txt_email') }}<br /> {{ trans('translate.email_1') }}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end of contact info -->

                <div class="container">
                    <!-- logo -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                            <i class="fa fa-bars"></i>
                        </button>
                        <a class="navbar-brand" href="{{ config('app.url') }}"><img src="{{ asset('img/pas-logo.png') }}" class="logo" alt=""></a>
                    </div>
                    <!-- end of logo -->
                    <!-- navigation -->
                    <div class="collapse navbar-collapse" id="navbar-menu">
                        <div class="main-nav-border"></div>
                        <ul class="nav navbar-nav navbar-left" data-in="fadeInDown" data-out="fadeOutUp">
{{--                            @php--}}
{{--                                $menuList = Menu::get(1);--}}
{{--                            @endphp--}}
{{--                            @foreach ($menuList as $mainmenu)--}}
{{--                                @if ($mainmenu['child'])--}}
{{--                                <li class="dropdown">--}}
{{--                                    <a href="{{ ($mainmenu['link'] == '#') ? '#' : LaravelLocalization::getLocalizedURL(Config::get('app.locale'), url($mainmenu['link'])) }}" class="dropdown-toggle" data-toggle="dropdown">--}}
{{--                                        @include('modules.translation', [--}}
{{--                                            'khmer'			=> $mainmenu['label_km'],--}}
{{--                                            'english'	    => $mainmenu['label_en'],--}}
{{--                                            'japanese'	    => $mainmenu['label_ja']--}}
{{--                                        ])--}}
{{--                                    </a>--}}
{{--                                    <ul class="dropdown-menu">--}}
{{--                                        @foreach ($mainmenu['child'] as $chileone)--}}
{{--                                            @if ($chileone['child'])--}}
{{--                                            <li class="dropdown">--}}
{{--                                                <a href="{{ ($chileone['link'] == '#') ? '#' : LaravelLocalization::getLocalizedURL(Config::get('app.locale'), url($chileone['link'])) }}" class="dropdown-toggle" data-toggle="dropdown">--}}
{{--                                                    @include('modules.translation', [--}}
{{--                                                        'khmer'			=> $chileone['label_km'],--}}
{{--                                                        'english'	    => $chileone['label_en'],--}}
{{--                                                        'japanese'	    => $chileone['label_ja']--}}
{{--                                                    ])--}}
{{--                                                </a>--}}
{{--                                                <ul class="dropdown-menu">--}}
{{--                                                    @foreach ($chileone['child'] as $chiletwo)--}}
{{--                                                    <li><a href="{{ ($chiletwo['link'] == '#') ? '#' : LaravelLocalization::getLocalizedURL(Config::get('app.locale'), url($chiletwo['link'])) }}">--}}
{{--                                                        @include('modules.translation', [--}}
{{--                                                            'khmer'			=> $chiletwo['label_km'],--}}
{{--                                                            'english'	    => $chiletwo['label_en'],--}}
{{--                                                            'japanese'	    => $chiletwo['label_ja']--}}
{{--                                                        ])--}}
{{--                                                    </a></li>--}}
{{--                                                    @endforeach--}}
{{--                                                </ul>--}}
{{--                                            </li>--}}
{{--                                            @else--}}
{{--                                            <li><a href="{{ ($chileone['link'] == '#') ? '#' : LaravelLocalization::getLocalizedURL(Config::get('app.locale'), url($chileone['link'])) }}">--}}
{{--                                                @include('modules.translation', [--}}
{{--                                                    'khmer'			=> $chileone['label_km'],--}}
{{--                                                    'english'	    => $chileone['label_en'],--}}
{{--                                                    'japanese'	    => $chileone['label_ja']--}}
{{--                                                ])--}}
{{--                                            </a></li>--}}
{{--                                            @endif--}}
{{--                                        @endforeach--}}
{{--                                    </ul>--}}
{{--                                </li>--}}
{{--                                @else--}}
{{--                                <li><a href="{{ ($mainmenu['link'] == '#') ? '#' : LaravelLocalization::getLocalizedURL(Config::get('app.locale'), url($mainmenu['link'])) }}">--}}
{{--                                    @include('modules.translation', [--}}
{{--                                        'khmer'			=> $mainmenu['label_km'],--}}
{{--                                        'english'	    => $mainmenu['label_en'],--}}
{{--                                        'japanese'	    => $mainmenu['label_ja']--}}
{{--                                    ])--}}
{{--                                </a></li>--}}
{{--                                @endif--}}
{{--                            @endforeach--}}
                        </ul>
                    </div>
                    <!-- end of navigation -->
                </div>
            </nav>
            <!-- end of nav -->

            <!-- start: page -->
            @yield('content')
            <!-- end: page -->

{{--            @php $list_partner = App\Partner::where('state', 1)->orderBy('ordering')->get() @endphp--}}
{{--            @if (!$list_partner->isEmpty())--}}
{{--            <!-- partner section -->--}}
{{--            <section id="testimonial" class="testimonial home1_testimonial sections">--}}
{{--                <div class="container">--}}
{{--                    <div class="row" style="position: relative">--}}
{{--                        <div class="main_testimonial home1_main_testimonial">--}}
{{--                            <div class="head_title text-center" style="position:relative;">--}}
{{--                                <h2>{!! trans('translate.partner') !!}</h2>--}}
{{--                                <div class="separator"></div>--}}
{{--                            </div>--}}
{{--                            <div class="col-md-12 no-padding" data-wow-delay="0.2s">--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-md-12">--}}
{{--                                        <div class="home1_clogo_area margin-top-30">--}}
{{--                                            <ol class="cline-logo-carousel-indicators">--}}
{{--                                                @for ($i = 0; $i < count($list_partner); $i++)--}}
{{--                                                <li>--}}
{{--                                                    @if (File::exists(public_path().'/images/partners/'.$list_partner[$i]->image) && ($list_partner[$i]->image))--}}
{{--                                                        <img src="{{ asset('images/partners/'.$list_partner[$i]->image) }}" alt="@include('modules.translation', ['khmer' => $list_partner[$i]->title_km, 'english' => $list_partner[$i]->title_en, 'japanese' => $list_partner[$i]->title_ja ])">--}}
{{--                                                    @else--}}
{{--                                                        <img src="{{ asset('img/no_image.png') }}" alt="@include('modules.translation', ['khmer' => $list_partner[$i]->title_km, 'english' => $list_partner[$i]->title_en, 'japanese' => $list_partner[$i]->title_ja ])">--}}
{{--                                                    @endif--}}
{{--                                                </li>--}}
{{--                                                @endfor--}}
{{--                                            </ol>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </section>--}}
{{--            <!-- end of partner section -->--}}
{{--            @endif--}}



            <!-- call us section -->
            <section id="callus" class="callus callus_home3">
                <div class="container">
                    <div class="row">
                        <div class="home3-overlay" style="position: absolute"></div>
                        <div class="main_call_us_area main_call_us_area_home3 textwhite">
                            <div class="col-sm-10">
                                <div class="call_us_left_text">
                                    <div class="row">
                                        <div class="col-xs-1">
                                            <div class="call_us_left_icon margin-top-10">
                                                <img src="{{ asset('img/h3subcribe.png') }}" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-xs-11">
                                            <h2 class="textwhite">{{ trans('translate.contact_headline') }}</h2>
                                            <p>{{ trans('translate.contact_text') }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="call_us_right_text">
                                    <a href="http://www.pas.gov.kh/contact" class="h33 btn btn-default">{{ trans('translate.contact') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end of call us section -->

            <!-- footer weidget -->
            <section id="footer_weidget" class="footer_weidget footer_weidget_home2" style="position: relative">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="main_f_weidget">
                                <div class="row">
                                    <div class="main_weidget main_weidget_home1 margin-top-80 margin-bottom-80">
                                        <!-- announce -->
                                        <div class="col-md-3 col-sm-6">
                                            <div class="single_weidget margin-top-40">
                                                <h5 class="margin-bottom-30">{{ trans('translate.announcement') }}</h5>
                                                <ul>
{{--                                                @php $list_announce = App\Announce::take(6)->where('state', 1)->orderBy('ordering')->get() @endphp--}}
{{--                                                @if (!$list_announce->isEmpty())--}}
{{--                                                    @for ($i = 0; $i < count($list_announce); $i++)--}}
{{--                                                    <li>--}}
{{--                                                        <a href="{{ LaravelLocalization::getLocalizedURL(Config::get('app.locale'), url('announce/'.$list_announce[$i]->id)) }}">--}}
{{--                                                        <i class="far fa-bullhorn pr-5"></i>--}}
{{--                                                        @include('modules.translation', [--}}
{{--                                                            'khmer'			=> $list_announce[$i]->alias_km ? $list_announce[$i]->alias_km : $list_announce[$i]->title_km,--}}
{{--                                                            'english'	    => $list_announce[$i]->alias_en ? $list_announce[$i]->alias_en : ($list_announce[$i]->title_en ? $list_announce[$i]->title_en : $list_announce[$i]->title_km),--}}
{{--                                                            'japanese'	    => ''--}}
{{--                                                            ])--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    @endfor--}}
{{--                                                @endif--}}
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- end of announce -->
                                        <!-- links -->
                                        <div class="col-md-3 col-sm-6">
                                            <div class="single_weidget margin-top-40">
                                                <h5 class="margin-bottom-30">{{ trans('translate.relatedwebsite') }}</h5>
                                                <ul>
{{--                                                @php $list_links = App\Link::where('state', 1)->where('featured', 1)->orderBy('ordering')->get() @endphp--}}
{{--                                                @if (!$list_links->isEmpty())--}}
{{--                                                    @for ($i = 0; $i < count($list_links); $i++)--}}
{{--                                                    <li>--}}
{{--                                                        <a href="{{ $list_links[$i]->link }}" target="_blank">--}}
{{--                                                        <i class="far fa-link pr-5"></i>--}}
{{--                                                        @include('modules.translation', [--}}
{{--                                                            'khmer'			=> $list_links[$i]->title_km,--}}
{{--                                                            'english'	    => $list_links[$i]->title_en ? $list_links[$i]->title_en : $list_links[$i]->title_km,--}}
{{--                                                            'japanese'	    => ''--}}
{{--                                                            ])--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                    @endfor--}}
{{--                                                @endif--}}
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- end of links -->
                                        <!-- news -->
                                        <div class="col-md-3 col-sm-6">
                                            <div class="single_weidget margin-top-40">
                                                <h5 class="margin-bottom-30">{{ trans('translate.social_participation') }}</h5>
{{--                                                @php $list_news = App\NewsCategory::find(2)->news()->take(5)->where('state', 1)->orderBy('ordering', 'desc')->get() @endphp--}}
{{--                                                @if (!$list_news->isEmpty())--}}
{{--                                                    @for ($i = 0; $i < count($list_news); $i++)--}}
{{--                                                        <div class="single_widget_blog">--}}
{{--                                                            <div class="row">--}}
{{--                                                                <div class="col-xs-3 no-padding">--}}
{{--                                                                    <div class="single_widget_img">--}}
{{--                                                                        @if (File::exists(public_path().'/images/news/'.$list_news[$i]->image) && ($list_news[$i]->image))--}}
{{--                                                                            <img src="{{ asset('images/news/'.$list_news[$i]->image) }}" alt="@include('modules.translation', ['khmer' => $list_news[$i]->alias_km ? $list_news[$i]->alias_km : $list_news[$i]->title_km, 'english' => $list_news[$i]->alias_en ? $list_news[$i]->alias_en : $list_news[$i]->title_en, 'japanese' => $list_news[$i]->alias_ja ? $list_news[$i]->alias_ja : $list_news[$i]->title_ja ])">--}}
{{--                                                                        @else--}}
{{--                                                                            <img src="{{ asset('img/no_image.png') }}" alt="@include('modules.translation', ['khmer' => $list_news[$i]->alias_km ? $list_news[$i]->alias_km : $list_news[$i]->title_km, 'english' => $list_news[$i]->alias_en ? $list_news[$i]->alias_en : $list_news[$i]->title_en, 'japanese' => $list_news[$i]->alias_ja ? $list_news[$i]->alias_ja : $list_news[$i]->title_ja ])">--}}
{{--                                                                        @endif--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                                <div class="col-xs-9">--}}
{{--                                                                    <div class="single_widget_content">--}}
{{--                                                                        <a href="{{ LaravelLocalization::getLocalizedURL(Config::get('app.locale'), url('news/'.$list_news[$i]->id)) }}">--}}
{{--                                                                            @include('modules.translation', [--}}
{{--                                                                                'khmer'			=> $list_news[$i]->alias_km ? Illuminate\Support\Str::limit($list_news[$i]->alias_km, 50) : Illuminate\Support\Str::limit($list_news[$i]->title_km, 50),--}}
{{--                                                                                'english'	    => $list_news[$i]->alias_en ? $list_news[$i]->alias_en : ($list_news[$i]->title_en ? lluminate\Support\Str::limit($list_news[$i]->title_en, 60) : Illuminate\Support\Str::limit($list_news[$i]->title_km, 50)),--}}
{{--                                                                                'japanese'	    => ''--}}
{{--                                                                            ])--}}
{{--                                                                        </a><br>--}}
{{--                                                                        <span class="datetime">--}}
{{--                                                                            @include('modules.day', [--}}
{{--                                                                                'day'	=> $list_news[$i]->created_at--}}
{{--                                                                            ])--}}
{{--                                                                            @include('modules.month', [--}}
{{--                                                                                'month'	=> $list_news[$i]->created_at--}}
{{--                                                                            ])--}}
{{--                                                                            @include('modules.year', [--}}
{{--                                                                                'year'	=> $list_news[$i]->created_at--}}
{{--                                                                            ])--}}
{{--                                                                        </span>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                    @endfor--}}
{{--                                                @endif--}}
                                            </div>
                                        </div>
                                        <!-- end of news -->
                                        <!-- address -->
                                        <div class="col-md-3 col-sm-6">
                                            <div class="single_weidget s_w_contact_us margin-top-40">
                                                <h5 class="margin-bottom-30">{{ trans('translate.contact') }}</h5>
                                                <div class="item_s_wcontact_us">
                                                    <div class="row">
                                                        <div class="col-xs-3 no-padding">
                                                            <div class="s_w_contact_us_icon text-center">
                                                                <span class="icon icon-Pointer"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-9">
                                                            <div class="s_w_contact_us_text">
                                                                <p>{{ trans('translate.address') }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item_s_wcontact_us margin-top-20">
                                                    <div class="row">
                                                        <div class="col-xs-3 no-padding">
                                                            <div class="s_w_contact_us_icon text-center">
                                                                <span class="icon icon-Phone2"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-9">
                                                            <div class="s_w_contact_us_text">
                                                                <p>{{ trans('translate.office') }},<br>
                                                                {{ trans('translate.telephone') }},<br>
                                                                {{ trans('translate.pilot') }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item_s_wcontact_us margin-top-20">
                                                    <div class="row">
                                                        <div class="col-xs-3 no-padding">
                                                            <div class="s_w_contact_us_icon text-center">
                                                                <span class="icon icon-Mail"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-9">
                                                            <div class="s_w_contact_us_text">
                                                                <p>{{ trans('translate.email_1') }},<br>
                                                                {{ trans('translate.email_2') }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end of address -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end of footer weidget -->

            <!-- copyright -->
            <footer class="footer">
                <div class="scrollup">
                    <a href="#"><i class="fa fa-chevron-up"></i></a>
                </div>

                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="main_footer">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="copyright_text text-center">
                                            <p class=" wow fadeInRight" data-wow-duration="1s">{!! trans('translate.copyright') !!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- end of copyright -->
        </div>

        <!-- JS includes -->
        <script src="{{ asset('js/vendor/jquery-1.11.2.min.js') }}"></script>
        <script src="{{ asset('js/vendor/bootstrap.min.js') }}"></script>

        <script src="{{ asset('js/jquery.magnific-popup.js') }}"></script>
        <script src="{{ asset('js/jquery.mixitup.min.js') }}"></script>
        <script src="{{ asset('js/jquery.easing.1.3.js') }}"></script>
        <script src="{{ asset('css/slick/slick.js') }}"></script>
        <script src="{{ asset('css/slick/slick.min.js') }}"></script>
        <script src="{{ asset('js/jquery.masonry.min.js') }}"></script>
        <script src="{{ asset('js/jquery.collapse.js') }}"></script>
        <script src="{{ asset('js/bootsnav.js') }}"></script>
        <script src="{{ asset('js/jquery-contact.js') }}"></script>

{{--        @if ($js)--}}
{{--            @for ($i = 0; $i < count($js); $i++)--}}
{{--                <script src="{{ asset($js[$i]) }}"></script>--}}
{{--            @endfor--}}
{{--        @endif--}}

        <script src="{{ asset('js/plugins.js') }}"></script>
        <script src="{{ asset('js/main.js') }}"></script>

    </body>
</html>
