@extends('template.master')

@section('content')

{{--@if (!$list_slideshow->isEmpty())--}}
<!-- slideshow section -->
<section id="home" class="home home1banner col-lg-12 col-xs-12 col-sm-12 col-md-12">
    <div class="container-fluid">
        <div class="row">
            <!-- bootstrap xsslider -->
            <div id="xsslider" class="carousel home1_slider xs_slide_spread xsswipe slide_xs_ease kenburns_zoomInOut xs_indicators_line xs_control_square" data-ride="carousel" data-pause="hover" data-interval="false" data-duration="1000">
{{--            <div id="xsslider" class="carousel home1_slider xs_slide_spread xsswipe slide_xs_ease xs_indicators_line xs_control_square" data-ride="carousel" data-pause="hover" data-interval="false" data-duration="1000">--}}
                <!-- wrapper for slides -->
                <div class="carousel-inner" role="listbox">
{{--                    @for ($i = 0; $i < count($list_slideshow); $i++)--}}
{{--                        @if (File::exists(public_path().'/images/slideshows/'.$list_slideshow[$i]->image) && ($list_slideshow[$i]->image))--}}
{{--                        <div class="item {{ $i == 0 ? 'active' : '' }}">--}}
{{--                            <img src="{{ asset('images/slideshows/'.$list_slideshow[$i]->image) }}" />--}}
{{--                            <div class="overlay"></div>--}}
{{--                            <div class="slide_style_3 slide_style_center">--}}
{{--                                <div class="home_slider_text textwhite">--}}
{{--                                    <h1>--}}
{{--                                        <span class="text-bg">--}}
{{--                                        @include('modules.translation', [--}}
{{--                                            'khmer'			=> str_replace_array(':', [' </span><span class="text-bg-blue">'], $list_slideshow[$i]->title_km),--}}
{{--                                            'english'	    => str_replace_array(':', [' </span><span class="text-bg-blue">'], $list_slideshow[$i]->title_en),--}}
{{--                                            'japanese'	    => ''--}}
{{--                                        ])--}}
{{--                                        </span>--}}
{{--                                    </h1>--}}
{{--                                    <p class="margin-top-20"><span class="text-bg">--}}
{{--                                        @include('modules.translation', [--}}
{{--                                            'khmer'			=> strip_tags($list_slideshow[$i]->brief_km, '<br>'),--}}
{{--                                            'english'	    => strip_tags($list_slideshow[$i]->brief_en ? $list_slideshow[$i]->brief_en : $list_slideshow[$i]->brief_km, '<br>'),--}}
{{--                                            'japanese'	    => ''--}}
{{--                                        ])--}}
{{--                                    </span></p>--}}
{{--                                    @if (!empty($list_slideshow[$i]->link1))--}}
{{--                                        <a href="{{ $list_slideshow[$i]->link1 }}" class="btn btn-dark m-r-15 margin-top-40" >--}}
{{--                                            @include('modules.translation', [--}}
{{--                                                'khmer'			=> $list_slideshow[$i]->name1_km,--}}
{{--                                                'english'	    => $list_slideshow[$i]->name1_en,--}}
{{--                                                'japanese'	    => ''--}}
{{--                                            ])--}}
{{--                                        </a>--}}
{{--                                    @endif--}}
{{--                                    @if(!empty($list_slideshow[$i]->link2))--}}
{{--                                        <a href="{{ $list_slideshow[$i]->link2 }}" class="btn btn-dark margin-top-40" >--}}
{{--                                            @include('modules.translation', [--}}
{{--                                                'khmer'			=> $list_slideshow[$i]->name2_km,--}}
{{--                                                'english'	    => $list_slideshow[$i]->name2_en,--}}
{{--                                                'japanese'	    => ''--}}
{{--                                            ])--}}
{{--                                        </a>--}}
{{--                                    @endif--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        @endif--}}
{{--                    @endfor--}}
                </div>
                <!-- end wrapper for slides -->
                <!-- left control -->
                <a class="left carousel-control" href="#xsslider" role="button" data-slide="prev">
                    <span class="fa fa-angle-left" aria-hidden="true"></span>
                </a>
                <!-- right control -->
                <a class="right carousel-control" href="#xsslider" role="button" data-slide="next">
                    <span class="fa fa-angle-right" aria-hidden="true"></span>
                </a>
            </div>
            <!-- end of bootstrap xsslider -->
        </div>
    </div>
</section>
<!-- end of slideshow section -->
{{--@endif--}}

<!-- featured section -->
<section id="featured" class="featured">
    <div class="container">
        <div class="row">
            <div class="main_featured main_featured_home1">
                <!-- ceo message -->
                <div class="col-md-8">
                    <div class="single_left_featured sections">
                        <h2>{!! trans('translate.message_title') !!}</h2>
                        <p>{!! trans('translate.message_text') !!}</p>
                        <div class="f_service_area margin-top-80">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="f_single_service">
                                        <div class="f_s_top_area">
                                            <div class="f_s_top_left">
                                                <div class="hexagon"><i class="far fa-eye"></i></div>
                                            </div>
                                            <div class="f_s_top_right text-right">
                                                <span>01</span>
                                            </div>
                                        </div>
                                        <div class="f_s_s_text margin-top-20">
                                            <h6>{{ trans('translate.vision_title') }}</h6>
                                            <p>{{ trans('translate.vision_text') }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="f_single_service">
                                        <div class="f_s_top_area">
                                            <div class="f_s_top_left">
                                                <div class="hexagon"><i class="far fa-bullseye-arrow"></i></div>
                                            </div>
                                            <div class="f_s_top_right text-right">
                                                <span>02</span>
                                            </div>
                                        </div>

                                        <div class="f_s_s_text margin-top-20">
                                            <h6>{{ trans('translate.mission_title') }}</h6>
                                            <p>{{ trans('translate.mission_text') }}</p>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="f_single_service">
                                        <div class="f_s_top_area">
                                            <div class="f_s_top_left">
                                                <div class="hexagon"><i class="far fa-handshake"></i></div>
                                            </div>
                                            <div class="f_s_top_right text-right">
                                                <span>03</span>
                                            </div>
                                        </div>
                                        <div class="f_s_s_text text-left margin-top-20">
                                            <h6>{{ trans('translate.values_title') }}</h6>
                                            <p>{{ trans('translate.values_text') }}</p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end of ceo message -->
                <!-- information inquire -->
                <div class="col-md-4">
{{--                    <div class="request_quot_area request_quot_area_home1" style="margin-top: 0px">--}}
                    <div class="request_quot_area request_quot_area_home1">
                        <div class="heading_request">
                            <div class="row">
                                <div class="col-xs-2"><i class="far fa-mail-bulk"></i></div>
                                <div class="col-xs-10">
                                    <h4>{{ trans('translate.information_inquire') }}</h4>
                                    <span>{{ trans('translate.feel_free_ask') }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="quote-form">
                            <div class="col-sm-12">
                                <div id="quote-error"></div>
                            </div>
                            <form method="post" action="scripts/quote.php" class="contact-form" id="quoteform">

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input type="text" id="name" name="name" class="form-control input-sm" placeholder="{{ trans('translate.name') }}">
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input type="email" class="form-control input-sm" name="email" id="email" placeholder="{{ trans('translate.email') }}">
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input type="subject" class="form-control input-sm" name="subject" id="subject" placeholder="{{ trans('translate.subject') }}">
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <textarea class="form-control input-sm" name="message" rows="3" placeholder="{{ trans('translate.message') }}"></textarea>
                                        </div>
                                    </div>

                                    <div class="col-sm-12 margin-top-20 text-center">
                                        <input type="submit" id="submit" value="{{ trans('translate.submit') }}" class="btn btn-lg">
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
{{--                    @isset ($row_video)--}}
{{--                    <!-- video promotion -->--}}
{{--                    <div class="youtube_video">--}}
{{--                        <div class="single_choose_video">--}}
{{--                            @if (File::exists(public_path().'/images/videos/'.$row_video->image) && ($row_video->image))--}}
{{--                                <img src="{{ asset('images/videos/'.$row_video->image) }}" data-toggle="modal" data-target="#modal1" alt="@include('modules.translation', ['khmer' => $row_video->alias_km ? $row_video->alias_km : $row_video->title_km, 'english' => $row_video->alias_en ? $row_video->alias_en : $row_video->title_en, 'japanese' => $row_video->alias_ja ? $row_video->alias_ja : $row_video->title_ja ])">--}}
{{--                            @else--}}
{{--                                <img src="{{ asset('img/no_image.png') }}" data-toggle="modal" data-target="#modal1" alt="@include('modules.translation', ['khmer' => $row_video->alias_km ? $row_video->alias_km : $row_video->title_km, 'english' => $row_video->alias_en ? $row_video->alias_en : $row_video->title_en, 'japanese' => $row_video->alias_ja ? $row_video->alias_ja : $row_video->title_ja ])">--}}
{{--                            @endif--}}
{{--                            <div class="s_choose_video_icon">--}}
{{--                                <a href="{{ $row_video->link }}" class="video-link"><i class="fal fa-play-circle"></i></a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <!-- end of video promotion -->--}}
{{--                    @endisset--}}
                </div>
                <!-- end of information inquire -->
            </div>
        </div>
    </div>
</section>
<!-- end of featured section -->

{{--@isset ($row_disclosure)--}}
{{--<!-- financial report -->--}}
{{--<section id="counter" class="counter counter_home3">--}}
{{--    <div class="home-overlay"></div>--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-sm-12">--}}
{{--                <div class="main_counter_area main_counter_area_home3 sections textwhite">--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-md-4">--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-xs-12">--}}
{{--                                    <div class="counter_left_top_heading_text">--}}
{{--                                        <h2 class="financial">{{ trans('translate.financial_report') }}</h2>--}}
{{--                                        <p>--}}
{{--                                        @include('modules.translation', [--}}
{{--                                            'khmer'			=> $row_disclosure->alias_km ? $row_disclosure->alias_km : $row_disclosure->title_km,--}}
{{--                                            'english'	    => $row_disclosure->alias_en ? $row_disclosure->alias_en : $row_disclosure->title_en,--}}
{{--                                            'japanese'	    => ''--}}
{{--                                        ])--}}
{{--                                        </p>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-xs-12 counter_item_area">--}}
{{--                                    <div class="row">--}}
{{--                                        <div class="col-sm-3">--}}
{{--                                            <div class="single_count_icon text-center margin-top-20">--}}
{{--                                                <i class="fal fa-comment-alt-dollar"></i>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-sm-9">--}}
{{--                                            <div class="single_counter_item">--}}
{{--                                                <h2 class="statistic-counter wow fadeInDown animated">{{ $row_disclosure->current_ratio }}</h2>--}}
{{--                                                <p class="wow fadeInUp text-uppercase animated">{{ trans('translate.current_ratio') }}</p>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-xs-12 margin-top-20">--}}
{{--                                    <div class="row">--}}
{{--                                        <div class="col-sm-3">--}}
{{--                                            <div class="single_count_icon text-center margin-top-20">--}}
{{--                                                <i class="fal fa-comment-dollar"></i>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-sm-9">--}}
{{--                                            <div class="single_counter_item">--}}
{{--                                                <h2 class="statistic-counter wow fadeInDown animated">{{ $row_disclosure->quick_ratio }}</h2>--}}
{{--                                                <p class="wow fadeInUp text-uppercase animated">{{ trans('translate.quick_ratio') }}</p>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-xs-12 margin-top-20">--}}
{{--                                    <div class="row">--}}
{{--                                        <div class="col-sm-3">--}}
{{--                                            <div class="single_count_icon text-center margin-top-20">--}}
{{--                                                <i class="fal fa-envelope-open-dollar"></i>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-sm-9">--}}
{{--                                            <div class="single_counter_item">--}}
{{--                                                <h2 class="statistic-counter wow fadeInDown animated">{{ $row_disclosure->earnings_share }}</h2>--}}
{{--                                                <p class="wow fadeInUp text-uppercase animated">{{ trans('translate.earnings_share') }}</p>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-8">--}}
{{--                            <div class="single_counter_right_text single_counter_right_text_home3">--}}
{{--                                <div class="single_counter_right_top_text">--}}
{{--                                    @include('modules.translation', [--}}
{{--                                        'khmer'			=> $row_disclosure->brief_km,--}}
{{--                                        'english'	    => $row_disclosure->brief_en,--}}
{{--                                        'japanese'	    => ''--}}
{{--                                    ])--}}
{{--                                    <a href="{{ LaravelLocalization::getLocalizedURL(Config::get('app.locale'), url('announce/'.$row_disclosure->id)) }}" class="readmore">Read More . .</a>--}}
{{--                                </div>--}}

{{--                                <div class="row margin-top-80">--}}
{{--                                    <div class="col-sm-3 col-xs-12">--}}
{{--                                        <div class="single_counter_item">--}}
{{--                                            <h3 class="wow fadeInDown animated"><span class="statistic-counter text-yellow">{{ $row_disclosure->return_asset }}</span>%</h3>--}}
{{--                                            <p class="wow fadeInUp text-uppercase animated">{{ trans('translate.return_asset') }}</p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="col-sm-3 col-xs-12">--}}
{{--                                        <div class="single_counter_item">--}}
{{--                                            <h3 class="wow fadeInDown animated"><span class="statistic-counter text-yellow">{{ $row_disclosure->return_equity }}</span>%</h3>--}}
{{--                                            <p class="wow fadeInUp text-uppercase animated">{{ trans('translate.return_equity') }}</p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="col-sm-3 col-xs-12">--}}
{{--                                        <div class="single_counter_item">--}}
{{--                                            <h3 class="wow fadeInDown animated"><span class="statistic-counter text-yellow">{{ $row_disclosure->gross_profit }}</span>%</h3>--}}
{{--                                            <p class="wow fadeInUp text-uppercase animated">{{ trans('translate.gross_profit') }}</p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="col-sm-3 col-xs-12">--}}
{{--                                        <div class="single_counter_item">--}}
{{--                                            <h3 class="wow fadeInDown animated"><span class="statistic-counter text-yellow">{{ $row_disclosure->net_profit }}</span>%</h3>--}}
{{--                                            <p class="wow fadeInUp text-uppercase animated">{{ trans('translate.net_profit') }}</p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}
{{--<!-- end offinancial report -->--}}
{{--@endisset--}}

{{--@if (!$list_news->isEmpty())--}}
{{--<!-- news section -->--}}
{{--<section id="blog" class="blog blog_home3">--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="main_blog main_blog_home3 sections">--}}
{{--                <div class="col-sm-12">--}}
{{--                    <div class="head_title text-center mb-0">--}}
{{--                        <h2>{!! trans('translate.news') !!}</h2>--}}
{{--                        <div class="separator"></div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                @for ($i = 0; $i < count($list_news); $i++)--}}
{{--                <div class="col-sm-6 col-md-4">--}}
{{--                    <div class="single_blog margin-top-50">--}}
{{--                        <div class="single_blog_img">--}}
{{--                        @if (File::exists(public_path().'/images/news/'.$list_news[$i]->image) && ($list_news[$i]->image))--}}
{{--                            <img src="{{ asset('images/news/'.$list_news[$i]->image) }}" alt="@include('modules.translation', ['khmer' => $list_news[$i]->alias_km ? $list_news[$i]->alias_km : $list_news[$i]->title_km, 'english' => $list_news[$i]->alias_en ? $list_news[$i]->alias_en : $list_news[$i]->title_en, 'japanese' => $list_news[$i]->alias_ja ? $list_news[$i]->alias_ja : $list_news[$i]->title_ja ])">--}}
{{--                        @else--}}
{{--                            <img src="{{ asset('img/no_image.png') }}" alt="@include('modules.translation', ['khmer' => $list_news[$i]->alias_km ? $list_news[$i]->alias_km : $list_news[$i]->title_km, 'english' => $list_news[$i]->alias_en ? $list_news[$i]->alias_en : $list_news[$i]->title_en, 'japanese' => $list_news[$i]->alias_ja ? $list_news[$i]->alias_ja : $list_news[$i]->title_ja ])">--}}
{{--                        @endif--}}
{{--                        </div>--}}
{{--                        <div class="single_blog_text">--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-xs-3">--}}
{{--                                    <div class="blog_recent_date text-center textwhite">--}}
{{--                                        <p>--}}
{{--                                        @include('modules.month', [--}}
{{--                                            'month'	=> $list_news[$i]->created_at--}}
{{--                                        ])--}}
{{--                                        </p>--}}
{{--                                        <div class="whitedividerauto"></div>--}}
{{--                                        <h4>--}}
{{--                                            @include('modules.day', [--}}
{{--                                                'day'	=> $list_news[$i]->created_at--}}
{{--                                            ])--}}
{{--                                        </h4>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-xs-9 no-padding">--}}
{{--                                    <div class="single_blog_text_left margin-top-20">--}}
{{--                                        <a href="{{ LaravelLocalization::getLocalizedURL(Config::get('app.locale'), url('news/'.$list_news[$i]->id)) }}">--}}
{{--                                        @include('modules.translation', [--}}
{{--                                            'khmer'			=> $list_news[$i]->alias_km ? $list_news[$i]->alias_km : $list_news[$i]->title_km,--}}
{{--                                            'english'	    => $list_news[$i]->alias_en ? $list_news[$i]->alias_en : ($list_news[$i]->title_en ? $list_news[$i]->title_en : $list_news[$i]->title_km),--}}
{{--                                            'japanese'	    => ''--}}
{{--                                        ])--}}
{{--                                        </a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                @endfor--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}
{{--<!-- end of section -->--}}
{{--@endif--}}






@endsection
