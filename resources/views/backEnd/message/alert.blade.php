<script>
    !function ($) {
        "use strict";
        var SweetAlert = function () {};
        SweetAlert.prototype.init = function () {
            @if (session('success'))
            $(window).load(function(){
                swal({
                    title: "Success !",
                    html: "{!! session('success') !!}",
                    type: "success",
                    showCancelButton: false,
                    cancelButtonClass: 'btn-default btn-md waves-effect',
                    confirmButtonClass: 'btn-success btn-md waves-effect waves-light',
                    confirmButtonText: 'OK'
                });
            });
            @endif
            @if (session('info'))
            $(window).load(function(){
                swal({
                    title: "Info !",
                    html: "{!! session('info') !!}",
                    type: "info",
                    showCancelButton: false,
                    cancelButtonClass: 'btn-default btn-md waves-effect',
                    confirmButtonClass: 'btn-info btn-md waves-effect waves-light',
                    confirmButtonText: 'OK'
                });
            });
            @endif
            @if (session('warning'))
            $(window).load(function(){
                swal({
                    title: "Warning !",
                    html: "{!! session('warning') !!}",
                    type: "warning",
                    showCancelButton: false,
                    cancelButtonClass: 'btn-default btn-md waves-effect',
                    confirmButtonClass: 'btn-warning btn-md waves-effect waves-light',
                    confirmButtonText: 'OK'
                });
            });
            @endif
            @if (session('error'))
            $(window).load(function(){
                swal({
                    title: "Error !",
                    html: "{!! session('error') !!}",
                    type: "error",
                    showCancelButton: false,
                    cancelButtonClass: 'btn-default btn-md waves-effect',
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: 'OK',
                });
            });
            @endif
        },
            //init
            $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
    }(window.jQuery),
        //initializing
        function ($) {
            "use strict";
            $.SweetAlert.init()
        }(window.jQuery);
</script>
