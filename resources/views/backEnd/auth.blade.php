<!DOCTYPE html>
<html>
	<head>
		<!-- Developer Info.
			Name: Sopheak SONG - (Smith)
			Email: songsopheak38@gmail.com
			Generator: Laravel v.7.2
			Summary: Dynamic Website, Responsive Design
		Developer Info. -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="Administrator, Laravel v.5.5" />
		<meta name="description" content="A fully featured administrator which can be used to manage your website.">
		<meta name="author" content="Sopheak SONG">
		<!-- App favicon -->
		<link rel="shortcut icon" href="{{ asset('backend/images/favicons/favicon.ico') }}" type="image/x-icon">
		<link rel="apple-touch-icon-precomposed" href="{{ asset('backend/images/favicons/apple-touch-icon.png') }}">
		<!-- App title -->
		<title>Authentication | Sihanoukville Autonomous Port.</title>
		@stack('header')
		<!-- App css -->
		<link href="{{ asset('backend/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('backend/css/core.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('backend/css/components.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('backend/css/icons.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('backend/css/pages.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('backend/css/menu.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('backend/css/responsive.css') }}" rel="stylesheet" type="text/css" />
		<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js') }}"></script>
		<![endif]-->
		<script src="{{ asset('backend/js/modernizr.min.js') }}"></script>
	</head>
	<body class="bg-transparent">
		<!-- HOME -->
		<section>
{{--            <div>--}}
{{--                <img src="{{ asset('backend/images/pas.jpg') }}" alt="" style="width: auto;height: auto;position:absolute !important;">--}}
{{--            </div>--}}
			<div class="container-alt">
				<div class="row">
					<div class="col-sm-12">
						<!-- start: page -->
						@yield('content')
						<!-- end: page -->
					</div>
				</div>
			</div>
		</section>
		<!-- END HOME -->
		<script>
		var resizefunc = [];
		</script>
		<!-- jQuery  -->
		<script src="{{ asset('backend/js/jquery.min.js') }}"></script>
		<script src="{{ asset('backend/js/bootstrap.min.js') }}"></script>
		<script src="{{ asset('backend/js/detect.js') }}"></script>
		<script src="{{ asset('backend/js/fastclick.js') }}"></script>
		<script src="{{ asset('backend/js/jquery.blockUI.js') }}"></script>
		<script src="{{ asset('backend/js/waves.js') }}"></script>
		<script src="{{ asset('backend/js/jquery.slimscroll.js') }}"></script>
		<script src="{{ asset('backend/js/jquery.scrollTo.min.js') }}"></script>
		@stack('footer')
		<!-- App js -->
		<script src="{{ asset('backend/js/jquery.core.js') }}"></script>
		<script src="{{ asset('backend/js/jquery.app.js') }}"></script>
		@include('backend.message.alert')
	</body>
</html>
