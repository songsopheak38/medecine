<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Database\Eloquent\Model;

class UsersTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
            'full_nme'    => 'songsopheak',
            'email'    => 'songsopheak@gmail.com',
            'password'   =>  \Illuminate\Support\Facades\Hash::make('123456'),
//            'remember_token' =>  str_random(10),
        ]);
    }
}
